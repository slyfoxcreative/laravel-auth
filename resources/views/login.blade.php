@extends('ui::layouts.app')

@section('title', 'Sign In')

@push('head')
  <meta name="turbo-cache-control" content="no-preview">
@endpush

@section('main')
  <x-bs-row :center="true" class="py-5">
    <x-bs-col size="auto">
      <x-html-form :action="route('login')" method="post">

        <x-bs-control name="username" class="mb-3">
          @label('username')
          @text('username', ['required' => true, 'autofocus' => true, 'autocomplete' => 'username'])
        </x-bs-control>

        <x-bs-control name="password" class="mb-3">
          @label('password')
          @password('password', ['required' => true, 'autocomplete' => 'current-password'])
        </x-bs-control>

        <x-bs-control type="checkbox" class="mb-3">
          @checkbox('remember')
          @label('remember', 'Remember me')
        </x-bs-control>

        <x-bs-button>
          @icon('right-to-bracket')
          Sign in
        </x-bs-button>

      </x-html-form>
    </x-bs-col>
  </x-bs-row>
@endsection
