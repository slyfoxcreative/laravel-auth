<?php

declare(strict_types=1);

namespace SlyFoxCreative\Auth\Ldap\Rules;

use LdapRecord\Laravel\Auth\Rule;

class OnlyImported extends Rule
{
    public function isValid()
    {
        return $this->model->exists;
    }
}
