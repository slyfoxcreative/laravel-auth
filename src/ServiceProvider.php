<?php

declare(strict_types=1);

namespace SlyFoxCreative\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Fortify;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-auth')
            ->hasViews()
            ->hasMigration('create_users_table')
        ;
    }

    public function packageBooted(): void
    {
        $this->configurePublishing();
        $this->configureFortify();
    }

    private function configurePublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../stubs/fortify.php' => config_path('fortify.php'),
            ], 'ui-config');

            $this->publishes([
                __DIR__ . '/../stubs/User.php' => app_path('Models/User.php'),
            ], 'ui-user');
        }
    }

    private function configureFortify()
    {
        Fortify::loginView(fn () => view('auth::login'));

        Fortify::authenticateUsing(function (Request $request) {
            $credentials = [
                'samaccountname' => $request->username,
                'password' => $request->password,
            ];

            if ($this->app->environment('local', 'testing')) {
                $credentials['fallback'] = $request->only('username', 'password');
            }

            $validated = Auth::validate($credentials);

            return $validated ? Auth::getLastAttempted() : null;
        });
    }
}
